DROP TABLE IF EXISTS tasks;
DROP TABLE IF EXISTS users;
DROP TABLE IF EXISTS tasks_users;
DROP TYPE IF EXISTS statusType;
DROP TYPE IF EXISTS roleType;


CREATE TYPE statusType AS ENUM ('new', 'in progress', 'done');
CREATE TYPE roleType AS ENUM ('user', 'admin');
CREATE TABLE IF NOT EXISTS tasks (
      id serial UNIQUE PRIMARY KEY,
      task_name VARCHAR(20) NOT NULL,
      complexity SMALLINT NOT NULL,
      status statusType DEFAULT 'new'
      );
CREATE TABLE IF NOT EXISTS users (
      id serial UNIQUE PRIMARY KEY,
      user_name VARCHAR(20) NOT NULL,
      role roleType NOT NULL,
      password VARCHAR(100) NOT NULL,
      email VARCHAR(50) UNIQUE  NOT NULL
      );
CREATE TABLE IF NOT EXISTS tasks_users (
      id serial UNIQUE PRIMARY KEY,
      task_id SMALLINT NOT NULL REFERENCES tasks(id),
      user_id SMALLINT NOT NULL REFERENCES users(id),
      UNIQUE (task_id, user_id)
    );

    -- const hash1 = await bcrypt.hash("111", 10);
    -- const hash2 = await bcrypt.hash("222", 10);
    -- const hash3 = await bcrypt.hash("333", 10);

INSERT INTO users (user_name, role, password, email) VALUES ('Ruslan', 'user', '111', 'rus@mail.com'), ('Alex', 'user', '222', 'alex@mail.com'), ('Dima', 'user', '333', 'dima@mail.com');
INSERT INTO tasks (task_name, complexity) VALUES('education', 5), ('reding', 10), ('coding', 10);
INSERT INTO tasks_users (task_id, user_id) VALUES(1, 2), (3, 1);
