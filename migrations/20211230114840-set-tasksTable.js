"use strict";

var dbm;
var type;
var seed;

/**
 * We receive the dbmigrate dependency from dbmigrate initially.
 * This enables us to not have to rely on NODE_PATH.
 */
exports.setup = function (options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function (db) {
  console.log("Start set tasks ===>>>");
  return Promise.all([
    db.insert("tasks", ["task_name", "complexity"], ["education", 5]),
    db.insert("tasks", ["task_name", "complexity"], ["reding", 10]),
    db.insert("tasks", ["task_name", "complexity"], ["coding", 10]),
  ]);
};

exports.down = function (db) {
  return null;
};

exports._meta = {
  version: 1,
};
