"use strict";

var dbm;
var type;
var seed;

/**
 * We receive the dbmigrate dependency from dbmigrate initially.
 * This enables us to not have to rely on NODE_PATH.
 */
exports.setup = function (options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function (db) {
  console.log("Start add tasks ===>>>");
  return db.createTable("tasks", {
    id: { type: "int", primaryKey: true, autoIncrement: true },
    task_name: { type: "string", notNull: true },
    complexity: { type: "int", notNull: true },
    status: { type: "string", defaultValue: "new" }, // enum
  });
};

exports.down = function (db) {
  return db.dropTable("tasks");
};

exports._meta = {
  version: 1,
};
