"use strict";

var dbm;
var type;
var seed;

/**
 * We receive the dbmigrate dependency from dbmigrate initially.
 * This enables us to not have to rely on NODE_PATH.
 */
exports.setup = function (options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function (db) {
  console.log("Start set tasks_users ===>>>");
  return Promise.all([
    db.insert("tasks_users", ["task_id", "user_id"], [1, 2]),
    db.insert("tasks_users", ["task_id", "user_id"], [3, 1]),
  ]);
};

exports.down = function (db) {
  return null;
};

exports._meta = {
  version: 1,
};
