"use strict";
const bcrypt = require("bcrypt");
var dbm;
var type;
var seed;

/**
 * We receive the dbmigrate dependency from dbmigrate initially.
 * This enables us to not have to rely on NODE_PATH.
 */
exports.setup = function (options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function (db) {
  console.log("Start set users ===>>>");
  // return db.insert("users", ["user_name", "role", "password", "email"],["Ruslan", "user", "111", "rus@mail.com"]);
  const hash1 = bcrypt.hashSync("111", 10);
  const hash2 = bcrypt.hashSync("222", 10);
  const hash3 = bcrypt.hashSync("333", 10);

  return Promise.all([
    db.insert(
      "users",
      ["user_name", "role", "password", "email"],
      ["Alex", "user", hash2, "alex@mail.com"]
    ),
    db.insert(
      "users",
      ["user_name", "role", "password", "email"],
      ["Dima", "user", hash3, "dima@mail.com"]
    ),
    db.insert(
      "users",
      ["user_name", "role", "password", "email"],
      ["Ruslan", "user", hash1, "rus@mail.com"]
    ),
  ]);
};

exports.down = function (db) {
  return null;
};

exports._meta = {
  version: 1,
};
