"use strict";

var dbm;
var type;
var seed;

/**
 * We receive the dbmigrate dependency from dbmigrate initially.
 * This enables us to not have to rely on NODE_PATH.
 */
exports.setup = function (options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function (db) {
  console.log("Start add tasks_users ===>>>");
  return db.createTable("tasks_users", {
    id: { type: "int", primaryKey: true, autoIncrement: true },
    task_id: {
      type: "int",
      foreignKey: {
        name: "tasks_users_task_id_fk",
        table: "tasks",
        rules: {
          onDelete: "CASCADE",
          onUpdate: "RESTRICT",
        },
        mapping: "id",
      },
    },
    user_id: {
      type: "int",
      foreignKey: {
        name: "tasks_users_user_id_fk",
        table: "users",
        rules: {
          onDelete: "CASCADE",
          onUpdate: "RESTRICT",
        },
        mapping: "id",
      },
    },
  });
};

exports.down = function (db) {
  return null;
};

exports._meta = {
  version: 1,
};
