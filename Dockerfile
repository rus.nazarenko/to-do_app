FROM node:14.17.3		

WORKDIR /server

COPY package*.json ./

RUN npm i

COPY . . 		

CMD npm run dev