export default {
  port: process.env.APP_PORT,
  secret: process.env.JWT_SECRET,
};
