import pool from "../db/index";
import bcrypt from "bcrypt";

const createTablesMW = async () => {
  try {
    const enumStatus =
      "CREATE TYPE statusType AS ENUM ('new', 'in progress', 'done')";
    const enumRole = "CREATE TYPE roleType AS ENUM ('user', 'admin')";

    const queryTasks = `CREATE TABLE IF NOT EXISTS tasks (
      id serial UNIQUE PRIMARY KEY,
      task_name VARCHAR(20) NOT NULL,
      complexity SMALLINT NOT NULL,
      status statusType DEFAULT 'new'
      )`;
    const queryUsers = `CREATE TABLE IF NOT EXISTS users (
      id serial UNIQUE PRIMARY KEY,
      user_name VARCHAR(20) NOT NULL,
      role roleType NOT NULL,
      password VARCHAR(100) NOT NULL,
      email VARCHAR(50) UNIQUE  NOT NULL
      )`;
    const queryTasksUsers = `CREATE TABLE IF NOT EXISTS tasks_users (
      id serial UNIQUE PRIMARY KEY,
      task_id SMALLINT NOT NULL REFERENCES tasks(id),
      user_id SMALLINT NOT NULL REFERENCES users(id),
      UNIQUE (task_id, user_id)
    )`;

    const hash1 = await bcrypt.hash("111", 10);
    const hash2 = await bcrypt.hash("222", 10);
    const hash3 = await bcrypt.hash("333", 10);

    const addUsers = `INSERT INTO users (user_name, role, password, email) VALUES ('Ruslan', 'user', '${hash1}', 'rus@mail.com'), ('Alex', 'user', '${hash2}', 'alex@mail.com'), ('Dima', 'user', '${hash3}', 'dima@mail.com')`;
    // INSERT INTO Products  (ProductName, Manufacturer, ProductCount, Price) VALUES ('iPhone 6', 'Apple', 3, 36000), ('Galaxy S8', 'Samsung', 2, 46000), ('Galaxy S8 Plus', 'Samsung', 1, 56000)
    const addTasks = `INSERT INTO tasks (task_name, complexity) VALUES('education', 5), ('reding', 10), ('coding', 10)`;
    const tasksUsers = `INSERT INTO tasks_users (task_id, user_id) VALUES(1, 2), (3, 1)`;

    // await pool.query("DROP TYPE IF EXISTS statusType;");
    // await pool.query("DROP TYPE IF EXISTS roleType;");
    await pool.query(enumStatus);
    await pool.query(enumRole);
    await pool.query(queryTasks);
    await pool.query(queryUsers);
    await pool.query(queryTasksUsers);
    await pool.query(addUsers);
    await pool.query(addTasks);
    await pool.query(tasksUsers);

    console.log("===>>> Migrations Ok <<<===");
  } catch (err) {
    console.log("Migrations error ===>>>", err);
  }
};

export default createTablesMW;
