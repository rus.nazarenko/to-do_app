import { Context, Next } from 'koa';
import srvConfig from '../config/server';
import jwt, { JwtPayload } from 'jsonwebtoken';
import pool from '../db/index';

const authMW = async (ctx: Context, next: Next) => {
  try {
    if (!ctx.request.header.token) throw new Error('no token');

    interface DecodedToken {
      user_name: string;
      email: string;
    }

    const token = ctx.request.header.token;
    const decodedToken: DecodedToken = jwt.verify(String(token), srvConfig.secret) as DecodedToken // !!! ???
    // const decodedToken2 = JSON.parse(JSON.stringify(decodedToken)); // !!! ???
    const sql = `SELECT id FROM users WHERE user_name=$1 AND email=$2`;
    const data = [decodedToken.user_name, decodedToken.email];
    const currentUser = await pool.query(sql, data);

    if (currentUser.rows[0]) {
      await next();
    } else {
      throw new Error('no user');
    }
  } catch (err) {
    ctx.response.status = 401;
    ctx.response.body = { auth: 'error', error: err.message };
  }
};

export default authMW;
