import fs from "fs";
import path from "path";
import morgan from "koa-morgan";

export const logging = () => {
  const accessLogStream = fs.createWriteStream(
    path.join(__dirname, "../log/access.log"),
    {
      flags: "a",
    }
  );

  return morgan("combined", { stream: accessLogStream });
};
