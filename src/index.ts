import { app } from './app';
import srv_cnf from './config/server';

const port = srv_cnf.port || 3002;

export const server = app.listen(port, () => {
  console.log(`Server is listening on port ${port}`);
});
