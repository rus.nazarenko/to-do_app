import pool from "../../db/index";
import bcrypt from "bcrypt";
import { PayloadNewUser } from "./userTypes";

export const addUserService = async (payload: PayloadNewUser) => {
  try {
    const hash = await bcrypt.hash(payload.password, 10);
    console.log("hash ===>>>", hash);

    const data = [payload.user_name, payload.role, hash, payload.email];
    const sql = `INSERT INTO users (user_name, role, password, email) VALUES ($1, $2, $3, $4) RETURNING id`;
    const newUser = await pool.query(sql, data);

    return newUser.rows[0].id;
  } catch (err) {
    throw new Error(err);
  }
};

export const getUserService = async (payload) => {
  try {
    const data = [payload];
    const sql = `SELECT id, user_name, role, email FROM users WHERE id=$1`;
    const user = await pool.query(sql, data);

    return user.rows[0];
  } catch (err) {
    throw new Error(err);
  }
};

export const getAllUsersServiсe = async () => {
  try {
    const sql = `SELECT id, user_name, email, role FROM users`;
    const users = await pool.query(sql);
    // console.log("getUsersServiсe ===>>>", users.rows);
    return users.rows;
  } catch (err) {
    throw new Error(err);
  }
};

export const getUsersServiсe = async (task_id: string) => {
  try {
    const data: any[] = [task_id];
    const sql: string = `SELECT u.id AS user_id, u.user_name, u.role, u.email  FROM tasks_users tu JOIN users u ON tu.user_id=u.id AND tu.task_id=$1`;
    const users = await pool.query(sql, data);
    // console.log("getUsersServiсe ===>>>", users.rows);
    return users.rows;
  } catch (err) {
    throw new Error(err);
  }
};

export const updateUserService = async ({
  id,
  user_name,
  email,
  role,
}: PayloadNewUser) => {
  try {
    const data = [user_name, email, role, id];
    const sql = `UPDATE users SET user_name=$1, email=$2, role=$3 WHERE id=$4`;
    const result = await pool.query(sql, data);
    // console.log("updateUserService", result);

    return "ok";
  } catch (err) {
    throw new Error(err);
  }
};
