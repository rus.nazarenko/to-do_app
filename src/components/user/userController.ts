import { Context } from "koa";
import {
  addUserService,
  getUsersServiсe,
  updateUserService,
  getAllUsersServiсe,
} from "./userService";
import { PayloadNewUser } from "./userTypes";

export const addUserController = async (ctx: Context) => {
  try {
    if (!ctx.request.body) throw new Error("No data");

    const payloadAddUser: PayloadNewUser = {
      user_name: ctx.request.body.user_name,
      role: ctx.request.body.role,
      password: ctx.request.body.password,
      email: ctx.request.body.email,
    };

    console.log(" role ===>>>", ctx.request.body.role, payloadAddUser.role);

    const newUserId = await addUserService(payloadAddUser);

    ctx.response.status = 201;
    ctx.response.body = { success: true, data: newUserId };
  } catch (err) {
    ctx.response.status = 401;
    ctx.response.body = { success: false, error: err.message };
  }
};

export const getAllUsersController = async (ctx: Context) => {
  try {
    const users = await getAllUsersServiсe();

    // console.log("getTasksController ===>>> ", tasks);
    ctx.response.status = 200;
    ctx.response.body = { success: true, data: users };
  } catch (err) {
    ctx.response.status = 400;
    ctx.response.body = { success: false, error: err };
  }
};

export const getUsersController = async (ctx: Context) => {
  try {
    const task_id: string = ctx.params.id;
    const users = await getUsersServiсe(task_id);

    // console.log("getTasksController ===>>> ", tasks);
    ctx.response.status = 200;
    ctx.response.body = { success: true, data: users };
  } catch (err) {
    ctx.response.status = 400;
    ctx.response.body = { success: false, error: err };
  }
};

export const updateUserController = async (ctx: Context) => {
  try {
    const modifiedUser: PayloadNewUser = {
      id: ctx.params.id,
      user_name: ctx.request.body.user_name,
      email: ctx.request.body.email,
      role: ctx.request.body.role,
    };

    const messg = await updateUserService(modifiedUser);

    ctx.response.status = 200;
    ctx.response.body = { success: true, data: messg };
  } catch (err) {
    ctx.response.status = 400;
    ctx.response.body = { success: false, error: err };
  }
};
