import Router from "koa-router";
import authMW from "../../middleware/authMW";
import {
  addUserController,
  getUsersController,
  updateUserController,
  getAllUsersController,
} from "./userController";

const userRouter = new Router({ prefix: "/user" });

userRouter.post("/", addUserController);
userRouter.get("/", authMW, getAllUsersController);
userRouter.get("/:id", authMW, getUsersController);
userRouter.put("/:id", authMW, updateUserController);

export default userRouter;
