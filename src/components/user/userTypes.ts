export enum Role {
  "user",
  "admin",
}

export interface PayloadNewUser {
  id?: string;
  user_name: string;
  role: Role;
  password?: string;
  email: string;
}
