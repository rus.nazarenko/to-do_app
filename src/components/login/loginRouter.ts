import Router from "koa-router";
import { loginController } from "./loginController";

const loginRouter = new Router({ prefix: "/login" });

loginRouter.post("/", loginController);

export default loginRouter;
