import { DataToken, LoginUser, ResData } from './loginTypes';
import bcrypt from 'bcrypt';
import pool from '../../db/index';
import jwt from 'jsonwebtoken';
import srvConfig from '../../config/server';

export const loginService = async ({ user_name, password }: LoginUser) => {
  try {
    const data = [user_name];
    const sql = `SELECT user_name, email, password FROM users WHERE user_name=$1`;
    const currentUser = await pool.query(sql, data);
    // console.log("passwd ===>>>", currentUser);

    const checkBcript = await bcrypt.compare(password, currentUser.rows[0].password);
    // console.log("checkBcript ===>>>" , checkBcript)

    const dataToken: DataToken = {
      user_name: currentUser.rows[0].user_name,
      email: currentUser.rows[0].email,
    };

    const token: string = jwt.sign(dataToken, srvConfig.secret);

    const resData: ResData = {
      access_token: token,
      user_name,
    };
    // console.log("resData ===>>> ", resData)

    return resData;
  } catch (err) {
    throw new Error(err);
  }
};
