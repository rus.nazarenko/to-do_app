export interface LoginUser {
  user_name: string;
  password: string;
}

export interface ResData {
  access_token: string;
  user_name: string;
}

export interface DataToken {
  user_name: string;
  email: string;
}
