import { Context } from "koa";
import { loginService } from "./loginService";
import { LoginUser, ResData } from "./loginTypes";

export const loginController = async (ctx: Context) => {
  try {
    const logUser: LoginUser = {
      user_name: ctx.request.body.user_name,
      password: ctx.request.body.password,
    };

    const resData: ResData = await loginService(logUser);

    ctx.response.status = 200;
    ctx.response.body = { success: true, data: resData };
  } catch (err) {
    ctx.response.status = 401;
    ctx.response.body = { success: false, error: err.message };
  }
};
