import Router from 'koa-router';
import authMW from '../../middleware/authMW';
import {
  addTaskController,
  delTaskController,
  getAllTasksController,
  getTasksController,
} from './taskController';

const taskRouter = new Router({ prefix: '/task' });

taskRouter.post('/', authMW, addTaskController);
taskRouter.get('/', authMW, getAllTasksController);
taskRouter.get('/:id', authMW, getTasksController);
taskRouter.del('/:id', authMW, delTaskController);

export default taskRouter;
