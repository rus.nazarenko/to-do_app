import request from 'supertest';
import jwt from 'jsonwebtoken';
import { getAllTasksService } from './taskService';
import { server } from '../../index';
import pool from '../../db/index';

const respBody = {
  success: true,
  data: [
    {
      id: 1,
      task_name: 'education',
      complexity: 5,
      status: 'new',
    },
  ],
};

jest.mock('./taskService', () => {
  const respData = [
    {
      id: 1,
      task_name: 'education',
      complexity: 5,
      status: 'new',
    },
  ];

  return {
    getAllTasksService: jest.fn().mockReturnValueOnce(respData),
  };
});

jest.mock('jsonwebtoken', () => {
  return { verify: jest.fn().mockReturnValueOnce(true) };
});

jest.mock('../../db/index', () => {
  return {
    query: jest.fn().mockReturnValueOnce({ rows: [{ id: 1 }] }),
  };
});

// let server;
// beforeAll(async () => {
//   const port = 3001;
//   server = app.listen(port, () => {
//     console.log(`Testserver is listening on port ${port}`);
//   });
//   // server = startServer();
// });
// afterAll(() => {
//   server.close();
// });

describe('TaskController', () => {
  test('Get all tasks', async () => {
    const response = await request(server).get('/task').set({
      token:
        'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiJSdXNsYW4iLCJlbWFpbCI6InJ1c0BtYWlsLmNvbSIsImlhdCI6MTY0MTk4NDU4OH0.eAmM8IeDDEUxc9aX2fPr8MaOF3uQ5d6YVwRRLAGYU7I',
    });
    // .send()
    // const res = await request(app).post('/register').send({ firstName: 'Jan' });
    expect(response.status).toBe(200);
    expect(response.body).toEqual(respBody);
    expect(getAllTasksService).toBeCalledTimes(1);
    expect(jwt.verify).toBeCalledTimes(1);
    expect(pool.query).toBeCalledTimes(1);
  });
});

// jest.mock('../user/userService', () => {
//   return {
//     getUserService: jest.fn().mockReturnValueOnce({
//       id: 1,
//       user_name: 'Ivan',
//       role: 'admin',
//       email: 'ivan@gmail.com',
//     }),
//   };
// });

// jest.mock('./taskService', () => {
//   return {
//     addTaskService: jest.fn().mockReturnValueOnce(1),
//   };
// });

// jest.mock('../tasks_users/task_usersService', () => {
//   return {
//     addTasksUsersService: jest.fn().mockReturnValueOnce({}),
//   };
// });

// describe('Task Controller', () => {
//   afterEach(() => {
//     jest.clearAllMocks();
//   });

//   it('should add a new task successfully', async () => {
//     const ctx = {
//       request: {
//         body: {
//           task_name: 'refactoring',
//           complexity: 7,
//           user_id: 8,
//         },
//       },
//       response: {},
//     };

//     const result = await addTaskController(ctx); // How to pass a ctx object???
//     expect(getUserService).toBeCalledTimes(1);
//     expect(getUserService).toBeCalledWith(ctx.request.body.user_id);
//     // expect(result).toEqual(newTaskResult);

//     const payloadNewTask = {
//       task_name: ctx.request.body.task_name,
//       complexity: ctx.request.body.complexity,
//     };

//     expect(addTaskService).toBeCalledTimes(1);
//     expect(addTaskService).toBeCalledWith(payloadNewTask);

//     const payloadTasksUsers = {
//       user_id: 1,
//       task_id: 1,
//     };

//     expect(addTasksUsersService).toBeCalledTimes(1);
//     expect(addTasksUsersService).toBeCalledWith(payloadTasksUsers);
//   });
// });
