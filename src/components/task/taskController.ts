import { Context } from "koa";
import { addTasksUsersService } from "../tasks_users/task_usersService";
import { getUserService } from "../user/userService";
import { addTaskService, delTaskService,
  getTasksService,  getAllTasksService } from "./taskService";
import { PayloadNewTask, PayloadTasksUsers } from "./taskTypes";

export const addTaskController = async (ctx: Context) => {
  try {
    if (!ctx.request.body) throw new Error("No data");

    // get user
    const user_id: number = ctx.request.body.user_id;
    const user: any = await getUserService(user_id);
    let newTaskId: number;

    // if the user exists - create a task
    if (user) {
      const payloadNewTask: PayloadNewTask = {
        task_name: ctx.request.body.task_name,
        complexity: ctx.request.body.complexity,
      };
      newTaskId = await addTaskService(payloadNewTask);
    } else {
      throw new Error("No user");
    }

    // create task and user relation
    const payloadTasksUsers: PayloadTasksUsers = {
      user_id: user.id,
      task_id: newTaskId,
    };

    await addTasksUsersService(payloadTasksUsers);

    ctx.response.status = 201;
    ctx.response.body = { success: true, data: newTaskId };
  } catch (err) {
    ctx.response.status = 401;
    ctx.response.body = { success: false, error: err.message };
  }
};

export const getAllTasksController = async (ctx: Context) => {
  try {
    const tasks = await getAllTasksService();

    // // console.log("getTasksController ===>>> ", tasks);
    ctx.response.status = 200;
    ctx.response.body = { success: true, data: tasks };
  } catch (err) {
    ctx.response.status = 400;
    ctx.response.body = { success: false, error: err };
  }
};

export const getTasksController = async (ctx: Context) => {
  try {
    const user_id: string = ctx.params.id;
    const tasks = await getTasksService(user_id);

    // console.log("getTasksController ===>>> ", tasks);
    ctx.response.status = 200;
    ctx.response.body = { success: true, data: tasks };
  } catch (err) {
    ctx.response.status = 400;
    ctx.response.body = { success: false, error: err };
  }
};

export const delTaskController = async (ctx: Context) => {
  try {
    const taskId: string = ctx.params.id;

    await delTaskService(taskId);
    ctx.response.status = 200;
    ctx.response.body = { success: true, data: "ok" };
  } catch (err) {
    ctx.response.status = 400;
    ctx.response.body = { success: false, data: err };
  }
};
