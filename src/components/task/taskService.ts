import pool from '../../db/index';
import { PayloadNewTask } from './taskTypes';

export const addTaskService = async ({
  task_name,
  complexity,
}: PayloadNewTask): Promise<number> => {
  try {
    const sql = 'INSERT INTO tasks (task_name, complexity) VALUES($1, $2) RETURNING id';
    const data = [task_name, complexity];

    console.log('addTaskService ===>>>', task_name, complexity);

    const newTask = await pool.query<{ id: number }>(sql, data);
    // console.log("addTaskService ===>>>", newTask);

    return newTask.rows[0].id;
  } catch (err) {
    // console.log("addTaskService err ===>>>", new Error(err));
    throw new Error(err);
  }
};

export const getAllTasksService = async () => {
  try {
    const sql = `SELECT id, task_name, complexity, status FROM tasks`;
    const tasks = await pool.query(sql);
    // console.log("getTasksServiсe ===>>>", tasks.rows);
    return tasks.rows;
  } catch (err) {
    throw new Error(err);
  }
};

export const getTasksService = async (user_id) => {
  try {
    const sql = `SELECT t.id AS task_id, task_name, complexity, status FROM tasks_users tu JOIN tasks t ON tu.task_id=t.id AND tu.user_id=$1`;
    const tasks = await pool.query(sql, [user_id]);
    // console.log("getTasksServiсe ===>>>", tasks.rows);
    return tasks.rows;
  } catch (err) {
    throw new Error(err);
  }
};

export const delTaskService = async (id) => {
  try {
    const sql = `DELETE FROM tasks WHERE id=$1`;
    const result = await pool.query(sql, [id]);
    // console.log("delTaskService ===>>>", result)
    return result.rows;
  } catch (err) {
    throw new Error(err);
  }
};
