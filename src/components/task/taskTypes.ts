export interface PayloadNewTask {
  task_name: string;
  complexity: number;
  // user_id: number;
}

export interface PayloadTasksUsers {
  user_id: number;
  task_id: number;
}
