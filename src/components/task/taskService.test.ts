import {
  addTaskService,
  getAllTasksService,
  getTasksService,
  delTaskService,
} from './taskService';
import pool from '../../db/index';

jest.mock('../../db/index', () => {
  return {
    query: jest.fn(),
  };
});

describe('Task Service', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should add a new task successfully', async () => {

    // arrange
    (pool.query as jest.Mock).mockReturnValueOnce({ rows: [{ id: 1 }] });

    const newTaskPayload = {
      task_name: 'coding',
      complexity: 10,
    };
    const newTaskResult = 1;
    const sql = 'INSERT INTO tasks (task_name, complexity) VALUES($1, $2) RETURNING id';
    const data = [newTaskPayload.task_name, newTaskPayload.complexity];

    // act
    const result = await addTaskService(newTaskPayload);

    // assert
    expect(pool.query).toBeCalledTimes(1);
    expect(pool.query).toBeCalledWith(sql, data);
    expect(result).toEqual(newTaskResult);
  });

  // it("should throw an error", async () => {
  //   expect(async () => {
  //     await addTaskService({task_name: "", complexity: 10, })
  //   }).toThrow();
  // });

  it('should get all tasks', async () => {
    // arrange

    (pool.query as jest.Mock).mockReturnValueOnce({
      rows: [
        {
          id: 17,
          task_name: 'coding',
          complexity: 10,
          status: 'new',
        },
      ],
    });

    const AddTaskResult = [
      {
        id: 17,
        task_name: 'coding',
        complexity: 10,
        status: 'new',
      },
    ];
    const sql = `SELECT id, task_name, complexity, status FROM tasks`;

    // act
    const result = await getAllTasksService();

    // assert
    expect(pool.query).toBeCalledTimes(1);
    expect(pool.query).toBeCalledWith(sql);
    expect(result).toEqual(AddTaskResult);
  });

  it('should get all tasks for the user with user_id=2', async () => {
    // arrange
    (pool.query as jest.Mock).mockReturnValueOnce({
          rows: [
            {
              task_id: 1,
              task_name: 'coding',
              complexity: 10,
              status: 'new',
            },
          ],
        })

    const getTaskResult = [
      {
        task_id: 1,
        task_name: 'coding',
        complexity: 10,
        status: 'new',
      },
    ];
    const user_id = 2;
    const sql = `SELECT t.id AS task_id, task_name, complexity, status FROM tasks_users tu JOIN tasks t ON tu.task_id=t.id AND tu.user_id=$1`;

    //act
    const result = await getTasksService(user_id);

    //assert
    expect(pool.query).toBeCalledTimes(1);
    expect(pool.query).toBeCalledWith(sql, [user_id]);
    expect(result).toEqual(getTaskResult);
  });

  it('should delete the task with task_id=1', async () => {
    // arrange
    (pool.query as jest.Mock).mockReturnValueOnce({
      rows: [
        {
          rowCount: 1,
          rows: [],
        },
      ],
    })
 
    const delTaskResult = [{ rowCount: 1, rows: [] }];
    const task_id = 1;
    const sql = `DELETE FROM tasks WHERE id=$1`;

    // act
    const result = await delTaskService(task_id);

    // assert
    expect(pool.query).toBeCalledTimes(1);
    expect(pool.query).toBeCalledWith(sql, [task_id]);
    expect(result).toEqual(delTaskResult);
  });
});
