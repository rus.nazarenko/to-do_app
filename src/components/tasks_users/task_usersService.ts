import pool from "../../db/index";

export const addTasksUsersService = async ({ user_id, task_id }) => {
  try {
    const data = [task_id, user_id];
    // console.log("addTasksUsersService ===>>>", user_id, task_id);
    const sql = `INSERT INTO tasks_users (task_id, user_id) VALUES($1, $2)`;

    const newTasksUsers = await pool.query(sql, data);
    // console.log("addTasksUsersService ===>>>", newTasksUsers);

    return newTasksUsers;
  } catch (err) {
    throw new Error(err);
  }
};
