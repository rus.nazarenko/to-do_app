import Koa, { Context, Next } from 'koa';
import taskRouter from './components/task/taskRouter';
import userRouter from './components/user/userRouter';
import srv_cnf from './config/server';
import bodyparser from 'koa-bodyparser';
import loginRouter from './components/login/loginRouter';
import { logging } from './middleware/loggingKoa';
// import { logging } from "./middleware/logging";

export const app = new Koa();

app.use(logging());
app.use(bodyparser());

// app.use(async (ctx, next) => {
//   try {
//     await next();
//   } catch (err) {
//     ctx.body = { ...err, message: err.message };
//     ctx.status = err.status || 500;
//   }
// });

app.use(taskRouter.routes());
app.use(userRouter.routes());
app.use(loginRouter.routes());

// export const startServer = async () => {
//   return await app.listen(port, () => {
//     console.log(`Server is listening on port ${port}`);
//   });
// };
// startServer();
